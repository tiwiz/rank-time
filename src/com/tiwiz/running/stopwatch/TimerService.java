package com.tiwiz.running.stopwatch;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created with IntelliJ IDEA.
 * User   : PC-Roby
 * Date   : 20/08/13
 * Time   : 15.53
 * Project: Running Stopwatch
 * Package: com.tiwiz.running.stopwatch
 */
public class TimerService extends Service
{
    private static long    initialTime;
    private        Handler handler;
    private Runnable    runnable;

    @Override
    public void onCreate()
    {
        initialTime = System.currentTimeMillis();
        handler = new Handler();

        runnable = new Runnable() {
            @Override
            public void run()
            {
                long currentTime = System.currentTimeMillis() - initialTime;

                Date newDate = new Date();
                newDate.setTime(currentTime);
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                String current = sdf.format(newDate);
                Intent timeUpdateIntent = new Intent(WatchActivity.TIME_SERVICE);
                timeUpdateIntent.putExtra(WatchActivity.TIME_VALUE,current);
                sendBroadcast(timeUpdateIntent);
                handler.postDelayed(this,1000);
            }
        };

        handler.post(runnable);
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    public void onDestroy()
    {
        handler.removeCallbacks(runnable);
        super.onDestroy();
    }
}
