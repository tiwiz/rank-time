package com.tiwiz.running.stopwatch;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User   : PC-Roby
 * Date   : 20/08/13
 * Time   : 11.12
 * Project: Running Stopwatch
 * Package: com.tiwiz.running.stopwatch
 */
public class TimesAdapter extends ArrayAdapter<Posizione>
{
    Context mContext;
    int layout;
    List<Posizione> listaPosizioni;
    PositionHolder holder = null;

    public TimesAdapter(Context context, int resource, List<Posizione> listaPosizioni)
    {
        super(context, resource, listaPosizioni);
        this.mContext = context;
        this.listaPosizioni = listaPosizioni;
        this.layout = resource;
    }

    public View getView(final int position, View convertView, ViewGroup parent){

        View row = convertView;

        if(row == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layout,parent,false);
            holder = new PositionHolder();
            holder.txtPettorina = (EditText)row.findViewById(R.id.txtPettorina);
            holder.txtPosizione = (TextView)row.findViewById(R.id.txtPosition);
            holder.txtTempo = (TextView)row.findViewById(R.id.txtTime);
            row.setTag(holder);
        }else
            holder = (PositionHolder)row.getTag();

        final Posizione posizione = listaPosizioni.get(position);
        holder.txtPosizione.setText(String.valueOf(posizione.getPosizione()));
        holder.txtTempo.setText(posizione.getTempo());

        int mPettorina = posizione.getPettorina();
        if(mPettorina != 0)
            holder.txtPettorina.setText(String.valueOf(mPettorina));
        else
            holder.txtPettorina.getText().clear();

        holder.txtPettorina.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3){}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3){}

            @Override
            public void afterTextChanged(Editable editable)
            {

                Posizione newDetail = listaPosizioni.get(position);
                newDetail.setPettorina(Integer.valueOf(editable.toString()));
                WatchActivity.listaPosizioni.remove(position);
                WatchActivity.listaPosizioni.add(position,newDetail);
            }
        });

        return row;
    }

    static class PositionHolder{
        TextView txtPosizione;
        TextView txtTempo;
        EditText txtPettorina;
    }
}
