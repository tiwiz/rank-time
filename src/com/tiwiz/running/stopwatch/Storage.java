package com.tiwiz.running.stopwatch;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.gson.Gson;
import java.util.ArrayList;


/**
 * Created with IntelliJ IDEA.
 * User   : PC-Roby
 * Date   : 20/08/13
 * Time   : 17.06
 * Project: Running Stopwatch
 * Package: com.tiwiz.running.stopwatch
 */
public class Storage
{
    private static final String PREF_ROOT_KEY = "com.tiwiz.running.stopwatch.preferences";
    private static final String PREF_RUNNERS_TEMP = "runnersTimesTemp";
    private static final String PREF_NOTIFICATION_ID = "notificationID";

    private static SharedPreferences getPrefs(){
        return WatchActivity.appContext.getSharedPreferences(PREF_ROOT_KEY, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getEdit(){
        SharedPreferences prefs = getPrefs();
        return prefs.edit();
    }

    private static void saveRunners(String data){

        SharedPreferences.Editor editor = getEdit();
        editor.putString(PREF_RUNNERS_TEMP,data);
        editor.commit();

    }

    public static void saveData(ArrayList<Posizione> data){
        Gson gson = new Gson();
        int size = data.size();
        String [] toJson = new String[size];

        for(int i = 0; i < size; i++)
            toJson[i] = data.get(i).toString();

        String json = gson.toJson(toJson);
        saveRunners(json);
    }

    public static void clearData(){
        saveRunners("");
    }

    public static ArrayList<Posizione> getData(){

        SharedPreferences prefs = getPrefs();
        String json = prefs.getString(PREF_RUNNERS_TEMP,"");
        ArrayList<Posizione> tempData = new  ArrayList<Posizione>();
        tempData.clear();

        Gson gson = new Gson();
        if(json.length() > 0){
            String[] tempValues = gson.fromJson(json,String[].class);

            for(int i = 0; i < tempValues.length; i++){
                Log.d("TIWIZ_JSON", tempValues[i]);
                Posizione p = new Posizione(tempValues[i]);
                tempData.add(p);
            }
        }

        return tempData;

    }
}
