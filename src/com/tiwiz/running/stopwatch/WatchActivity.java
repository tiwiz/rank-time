package com.tiwiz.running.stopwatch;

import android.app.*;
import android.content.*;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.*;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.*;
import android.widget.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class WatchActivity extends Activity implements View.OnClickListener
{
    /**
     * Called when the activity is first created.
     */

    private       Button               btnTime;
    private       ListView             timeList;
    private       TextView             txtTimeWidget;
    public static ArrayList<Posizione> listaPosizioni;
    public static Context appContext;
    private       TimesAdapter         adapter;
    private BroadcastReceiver timeUpdateReceiver;
    private BroadcastReceiver listUpdateReceiver;
    private static int nextPosition = 1;

    public final static String TIME_SERVICE = "com.tiwiz.running.stopwatch.timeUpdateService";
    public final static String TIME_VALUE = "com.tiwiz.running.stopwatch.timeValue";


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //keeps screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        appContext = this;

        btnTime = (Button)findViewById(R.id.btnTime);
        btnTime.setOnClickListener(this);

        timeList = (ListView)findViewById(R.id.listTimes);
        //listaPosizioni = new ArrayList<Posizione>();
        //listaPosizioni.clear();
        listaPosizioni = Storage.getData();

        View header = getLayoutInflater().inflate(R.layout.list_header,null);
        timeList.addHeaderView(header);

        adapter = new TimesAdapter(this,R.layout.list_element,listaPosizioni);
        timeList.setAdapter(adapter);

        txtTimeWidget = (TextView)findViewById(R.id.txtTimeUpdate);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();

        timeUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                String currentTime = intent.getStringExtra(TIME_VALUE);

                if(currentTime.length() == 0)
                    currentTime = getString(R.string.time_test_string);

                txtTimeWidget.setText(currentTime);
            }
        };

        IntentFilter filter = new IntentFilter(TIME_SERVICE);
        registerReceiver(timeUpdateReceiver,filter);

        //TODO controllare

    }

    public void onPause(){
        super.onPause();
        unregisterReceiver(timeUpdateReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
        case R.id.btnStart:
            startTimer();
            break;
        case R.id.btnStop:
            stopTimer();
            break;
        case R.id.btnShare:
            shareResults();
            break;
        case R.id.btnDelete:
            clearData();
            break;
        default: break;
        }

        return true;
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId()){
        case R.id.btnTime:
            takeTime();
            break;
        }
    }

    private void startTimer(){
        startService(new Intent(this,TimerService.class));
    }

    private void stopTimer(){
        stopService(new Intent(this, TimerService.class));
        txtTimeWidget.setText(R.string.time_test_string);
    }

    public void takeTime(){

        Posizione newPosizione = new Posizione(nextPosition,txtTimeWidget.getText().toString());

        listaPosizioni.add(newPosizione);

        adapter.notifyDataSetChanged();
        Storage.saveData(listaPosizioni);

        nextPosition++;
    }

    private void shareResults(){

        ExportData dataExp = new ExportData();
        dataExp.execute();

    }

    private void clearData(){

        DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int button)
            {
               switch(button){
               case DialogInterface.BUTTON_POSITIVE:
                   stopTimer();
                   Storage.clearData();
                   listaPosizioni.clear();
                   adapter.notifyDataSetChanged();
                   nextPosition = 1;
               default:
                   break;
               }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.delete_data_title)
                .setMessage(R.string.delete_data_message)
                .setPositiveButton(R.string.delete_data_yes,clickListener)
                .setNegativeButton(R.string.delete_data_no,clickListener)
                .setIcon(R.drawable.alert_warning)
                .setCancelable(true);

        builder.show();
    }


    private class ExportData extends AsyncTask<Void,Void,Void>{
        ProgressDialog progressDialog;
        String filename = "";
        String filepath = "";

        @Override
        protected void onPreExecute()
        {
            String title = getString(R.string.exporting_data_title);
            String message = getString(R.string.exporting_data_message);
            progressDialog = ProgressDialog.show(WatchActivity.this,title,message, true);

            Calendar cal = Calendar.getInstance();
            StringBuilder bfilename = new StringBuilder();
            bfilename.append(getString(R.string.exporting_data_filename_header));
            bfilename.append(cal.get(Calendar.YEAR));
            bfilename.append(cal.get(Calendar.MONTH));
            bfilename.append(cal.get(Calendar.DAY_OF_MONTH));
            bfilename.append(".csv");

            filename = bfilename.toString();
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            String fileHeader = getString(R.string.exporting_data_table_header);

            File sd = Environment.getExternalStorageDirectory();
            File f = new File(sd,filename);

            filepath = f.getAbsolutePath();

            FileWriter fw = null;
            BufferedWriter bw = null;

            try
            {
                fw = new FileWriter(f);
                bw = new BufferedWriter(fw);
                bw.write(fileHeader);
                bw.newLine();
                for(Posizione p : listaPosizioni){
                    StringBuilder line = new StringBuilder();
                    line.append(p.getPosizione());
                    line.append(";");
                    line.append(p.getTempo());
                    line.append(";");
                    line.append(p.getPettorina());

                    bw.write(line.toString());
                    bw.newLine();
                    bw.flush();
                }

                bw.close();
                fw.close();

            }
            catch (IOException e)
            {
                //do nothing
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            super.onPostExecute(result);
            progressDialog.dismiss();

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int button)
                {
                    switch(button){
                    case DialogInterface.BUTTON_POSITIVE: //open
                        Intent openIntent = new Intent(Intent.ACTION_VIEW);
                        openIntent.setDataAndType(Uri.parse(filepath),"text/csv");
                        startActivity(openIntent);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE: //send
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/csv");
                        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(filepath));
                        WatchActivity.this.startActivity(Intent.createChooser(shareIntent,"Invia"));
                        break;
                    default: break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(WatchActivity.this)
                    .setTitle(R.string.exporting_data_title)
                    .setMessage(getString(R.string.data_exported_successfully))
                    .setCancelable(true)
                    .setIcon(R.drawable.alert_info)
                    .setPositiveButton(getString(R.string.btn_yes_open),listener)
                    .setNegativeButton(getString(R.string.btn_no_share),listener);

            builder.show();
        }
    }

    public void updateList(int pos, int detail){

        Posizione p = listaPosizioni.get(pos);
        p.setPettorina(detail);
        listaPosizioni.add(pos,p);

        Log.d("TIWIZ_JSON", "Lista aggiornata");
    }
}
