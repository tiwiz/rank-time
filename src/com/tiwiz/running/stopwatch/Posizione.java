package com.tiwiz.running.stopwatch;

/**
 * Created with IntelliJ IDEA.
 * User   : PC-Roby
 * Date   : 20/08/13
 * Time   : 11.06
 * Project: Running Stopwatch
 * Package: com.tiwiz.running.stopwatch
 */
public class Posizione
{
    private int posizione;
    private String tempo;
    private int pettorina;

    public Posizione(int posizione, String tempo, int pettorina){
        this.posizione = posizione;
        this.tempo = tempo;
        this.pettorina = pettorina;
    }

    public Posizione(int posizione,String tempo){
        this(posizione, tempo, 0);
    }

    public Posizione(String init){

        String[] values = init.split(";");
        this.posizione = Integer.valueOf(values[0]);
        this.tempo = values[1];
        this.pettorina = Integer.valueOf(values[2]);
    }

    public int getPosizione()
    {
        return posizione;
    }

    public void setPosizione(int posizione)
    {
        this.posizione = posizione;
    }

    public String getTempo()
    {
        return tempo;
    }

    public void setTempo(String tempo)
    {
        this.tempo = tempo;
    }

    public int getPettorina()
    {
        return pettorina;
    }

    public void setPettorina(int pettorina)
    {
        this.pettorina = pettorina;
    }

    public String toString(){
        return posizione + ";" + tempo + ";" + pettorina;
    }
}
